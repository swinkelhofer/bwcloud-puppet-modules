class nfs::custom_docker_client (
	$server = ""
) {
	class { '::nfs':
		client_enabled => true,
	}
	Nfs::Client::Mount <<|nfstag == 'docker' |>> {
		server => $nfs::custom_docker_client::server,
		ensure	=> 'mounted',
		mount => '/export',
	}
	Nfs::Client::Mount <<|nfstag == 'exp' |>> {
		server => $nfs::custom_docker_client::server,
		ensure	=> 'mounted',
		mount => '/export',
	}
}
