class nfs::custom_server  {
	class { '::nfs':
		server_enabled => true
	}
	nfs::server::export{ '/exp':
   		 ensure  => 'mounted',
   		 clients => '192.168.0.0/24(rw,insecure,async,no_root_squash) localhost(rw) 172.17.0.0/16(rw,insecure,async,no_root_squash) 10.0.0.0/8(rw,insecure,async,no_root_squash)',
		nfstag  => 'exp',
		mount   => '/export',
	}
	nfs::server::export{ '/docker':
   		 ensure  => 'mounted',
   		 clients => '192.168.0.0/24(rw,insecure,async,no_root_squash) localhost(rw) 172.17.0.0/16(rw,insecure,async,no_root_squash) 10.0.0.0/8(rw,insecure,async,no_root_squash)',
		 nfstag =>  'docker',
		 mount  =>  '/var/lib/docker',
	}
}
