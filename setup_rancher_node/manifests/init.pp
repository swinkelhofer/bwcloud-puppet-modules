class setup_rancher_node (
	$management = rancher::node::management,
	$registration_token = $rancher::node::registration_token,
	$nfs_server = ""
) {
	include stdlib
	include docker
	# /var/lib/docker Mountpoint handling
	file { '/var/lib/docker':
		ensure	=> 'directory',
		owner	=> 'root',
		group 	=> 'root',
		mode	=> '0711',
	}
	exec { '/sbin/mkfs.ext4 -F /dev/vdb':
		onlyif	=> 'file -sL /dev/vdb | grep -v ext4',
		provider=> 'shell',
	}
	exec { 'sed -i "s:Port 22$:Port 2222:g" /etc/ssh/sshd_config':
		#require => File['/etc/ssh/sshd_config'],
		provider=> 'shell',
	}
	exec { '/bin/mount -t ext4 /dev/vdb /var/lib/docker':
		require	=> File['/var/lib/docker'],
		provider => 'shell',
		onlyif	=> ['mountpoint /var/lib/docker | grep not', 'file -sL /dev/vdb | grep ext4'],
	}
	file_line { 'vdb-mount':
		path	=> '/etc/fstab',
		line	=> '/dev/vdb /var/lib/docker ext4 rw 0 0',
	}
	
	# NFS /export Mountpoint handling
	package { 'nfs-kernel-server':
		ensure => present,
	}
	file { '/export':
		ensure	=> 'directory',
		owner	=> 'root',
		group 	=> 'root',
		mode	=> '0711',
	}
	exec { "/bin/mount -t nfs $setup_rancher_node::nfs_server:/export/data /export":
		require	=> File['/export'],
		provider => 'shell',
		onlyif	=> ['mountpoint /export | grep not'],
	}
	file_line { 'nfs-mount':
		path	=> '/etc/fstab',
		line	=> "$setup_rancher_node::nfs_server:/export/data /export nfs rw 0 0",
	}

	# Register Rancher Node	
	class { 'rancher::node':
		management	=> $setup_rancher_node::management,
		registration_token => $setup_rancher_node::registration_token,
	}
}
