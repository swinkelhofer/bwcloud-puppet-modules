class setup_nfs_server (
) {
	include stdlib
	
	# SSH Port 2222
	exec { 'sed -i "s:Port 22$:Port 2222$:g" /etc/ssh/sshd_config':
                provider=> 'shell',
        }

	# Mount external volume on /dev/vdb
	exec { '/sbin/mkfs.ext4 -F /dev/vdb':
		onlyif	=> 'file -sL /dev/vdb | grep -v ext4',
		provider=> 'shell',
	}
	exec { '/bin/mount -t ext4 /dev/vdb /export':
		require	=> File['/export'],
		provider => 'shell',
		onlyif	=> ['mountpoint /export | grep not', 'file -sL /dev/vdb | grep ext4'],
	}
	file_line { 'vdb-mount':
		path	=> '/etc/fstab',
		line	=> '/dev/vdb /export ext4 rw 0 0',
	}

	# NFS Definition
	file { '/export':
		ensure	=> 'directory',
		owner	=> 'root',
		group 	=> 'root',
		mode	=> '0711',
	}
	class { '::nfs':
                server_enabled => true,
        }
	nfs::server::export{ '/export/data':
                 ensure  => 'mounted',
                 clients => '192.168.0.0/24(rw,insecure,async,no_root_squash) localhost(rw) 172.17.0.0/16(rw,insecure,async,no_root_squash) 10.0.0.0/8(rw,insecure,async,no_root_squash)',
                nfstag  => 'data',
                mount   => '/export',
        }

	# Attic Backup	
	package { 'attic':
		ensure	=> 'installed',
	}
	package { 'sshfs':
		ensure	=> 'installed',
	}
	vcsrepo { '/root/scripts':
		ensure	=> 'present',
		provider	=> 'git',
		source	=> 'https://gitlab.com/swinkelhofer/zawiw-scripts.git',
	}
	#cron { 'attic-backup':
	#	command	=> '/bin/bash /root/scripts/attic.sh',
	#	user	=> 'root',
	#	hour	=> 1,
	#	minute	=> 59,
	#}	
	file { '/root/backup':
		ensure	=> 'directory',
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0755',
	}

}
