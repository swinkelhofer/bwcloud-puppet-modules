# Management Server.
class rancher::management(
  $rancher_manager_port = '8080',
){
  require docker

  docker::image { 'rancher/server':
    image_tag => 'v1.6.18',
  }
  docker::image { 'zawiw/mysql' :
  }
  docker::run { 'rancher/mysql':
    image => 'mariadb',
    env => ['MYSQL_ROOT_PASSWORD=secret', 'MYSQL_DATABASE=cattle', 'MYSQL_USER=cattle', 'MYSQL_PASSWORD=cattle'],
    volumes  => ['/export/rancher/dump.sql.gz:/docker-entrypoint-initdb.d/dump.sql.gz'],
    require => Docker::Image['zawiw/mysql'],
  }
  docker::run { 'rancher/server':
    image   => 'rancher/server:v1.6.15',
    links   => 'rancher-mysql:rancher-mysql',
    env => ['CATTLE_DB_CATTLE_MYSQL_HOST=rancher-mysql', 'CATTLE_DB_CATTLE_MYSQL_NAME=cattle', 'CATTLE_DB_CATTLE_USERNAME=cattle', 'CATTLE_DB_CATTLE_PASSWORD=cattle'],
    ports   => [ "${rancher_manager_port}:8080" ],
    require => [ Docker::Image['rancher/server'], Docker::Run['rancher/mysql'] ],
  }
}

